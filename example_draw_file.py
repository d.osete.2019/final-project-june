#!/usr/bin/env python3

import sources
import sinks

def main():
    sound = sources.load('recording.wav')
    sinks.draw(sound, 30)

if __name__ == '__main__':
    main()